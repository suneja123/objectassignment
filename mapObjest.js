function mapObject(obj, cb) {
    // Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
    // http://underscorejs.org/#mapObject

    let mapedArray = [];

   for(let prep of Object.keys(obj)){

   
        mapedArray.push(cb(obj[prep]));
       
   }
  
    return mapedArray;
}

module.exports = {mapObject};
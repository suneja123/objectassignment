


function invert(obj) {

    let invertedArray = [];
    let key = Object.keys(obj);
    let val = Object.values(obj);
    for(let i=0;i<key.length;i++)
    {
       invertedArray[i] = [key[i],val[i]];
    }

    return invertedArray;

}
module.exports={ invert };